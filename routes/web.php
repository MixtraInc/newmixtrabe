<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

// Create or Register user.

$app->post('/user', 'MembersController@createMember');

// Protected routes

$app->group(['middleware' => 'auth:api'], function () use ($app) {

	// Get user logged user info.

	$app->get('/user', 'MembersController@getMember');

	// Create posts.

	$app->post('/posts', 'PostsController@createPost');

	// Retrieve Posts.

	$app->get('/posts', 'PostsController@retrievePosts');

	// Follow users.

	$app->post('/follow', 'FollowersController@follow');

	// Retrieve followings.

	$app->get('/followings', 'FollowersController@retrieveFollowings');

	// Check if already folllowing.

	$app->get('/follow/{followingid}', 'FollowersController@checkRelation');




});