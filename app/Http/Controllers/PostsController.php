<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Http\Controllers\MembersController As Members;
use App\Http\Controllers\FollowersController As Followers;
use App\Post;


class PostsController extends Controller
{
    

    // Create post.

    public function createPost(Request $request){

    	// Return validation error

        try{

            $this->validateRequest($request);

        } catch(ValidationException $e){

            return $e->getResponse()->setStatusCode(400);
        }
    	

		$post_container 	= 	new Post;
		$authorid	        =	$request->user()->userid;
        $first_name         =   $request->user()->fname;
        $last_name          =   $request->user()->lname;
		$author 	        =	$first_name. " " . $last_name;
		

        $post_container->userid 	  = $authorid;
		$post_container->author 	  = $author;
		$post_container->content 	  = $request->body;

        if ($request->has('lname')) {
            $post_container->place    = $request->place;;
        }

		//$post->link_file  = $request->files;

		$post_container->save();

    	   			
    	return response(array('status' => 'success'), 201);

    }


    // Retrieve Posts by follower.


    public function retrievePosts(Request $request){

    	$post 	 	= 	new Post;

    	$followers 	= 	new Followers;

    	$followings =	$followers->retrieveFollowings($request);

    	$authorid	=	$request->user()->userid;

        if($followings['following'] == NULL){

        	$res 		= $post->Where('userid', $authorid)
        							->latest()
        								->get();
        }

        else{

            $res         = $post->whereIn('userid', $followings['following'] )
                                ->orWhere('userid', $authorid)
                                    ->latest()
                                        ->get(); 
        }

        return array('posts' => $res);

    }


    // Validate input.

    private function validateRequest(Request $request){

        $rules = [
            'place' => 'bail|max:100',
            'body' => 'bail|required|min:1|max:4000',
        ];

        $this->validate($request, $rules);

    }
        
}
