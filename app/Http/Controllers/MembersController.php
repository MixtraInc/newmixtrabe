<?php

namespace App\Http\Controllers;

// Classes.

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
Use Illuminate\Validation\ValidationException;

// Models.

use App\Member;


class MembersController extends Controller
{
    //Create User.

    public function createMember(Request $request){

        try{

            $this->validateRequest($request);

        } catch(ValidationException $e){

            return $e->getResponse()->setStatusCode(400);
        }

            
        $member = new Member;
        $member->fname    =  $request->fname;
        
        if ($request->has('lname')) {
            $member->lname    =  $request->lname;
        }

        $member->email    =  $request->email;
        $member->mobile   =  $request->mobile;
        $member->userid   =  $userid = md5($request->email);
        $member->password =  Hash::make($request->password);

        $t = $member->save();

        $res = array('status' => 'Success','userid' => $userid);

        return response($res, 201)
              ->header('user-id', $userid);
        
    }   



    /* Get user details with id.
     *
     */

    public function getMember(Request $request){

        $res = array(

                 'id' => $request->user()->userid,
                 'first_name' => $request->user()->fname,
                 'last_name' => $request->user()->lname,
                 'email' => $request->user()->email,
                 'mobile' => $request->user()->mobile

                );

        return response(array('status' => 'Success', 'user' => $res), 200);
    }


    public function getMemberInfo($userid){

        $members = new Member;

        $member = $members->where('userid', $userid)->first();

        //$member = $member->toArray();

        $res    = ['name' => $member->fname.' '.$member->lname ];

        return $res;

    }


    // Validate request.

    private function validateRequest(Request $request){

        $rules = [
            'fname' => 'bail|required|max:30|alpha',
            'lname' => 'bail|sometimes|max:30|alpha',
            'email' => 'bail|required|email|max:60|unique:members',
            'mobile' => 'bail|required|digits_between:4,13|unique:members',
            'password' => 'bail|required|min:6',
        ];

        $this->validate($request, $rules);

    }

}
