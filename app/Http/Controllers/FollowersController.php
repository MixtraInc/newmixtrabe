<?php

namespace App\Http\Controllers;
 
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Follower;


class FollowersController extends Controller
{
    // Follow.

    Public function follow(Request $request){

    	$follower 			= $request->user()->userid;
    	$following 		 	= $request->input('followingid');

    	$data = [ 'follower' => $follower, 'following' => $following ];

    	
    	try{

            $this->validateRequest($request);

        } catch(ValidationException $e){

            return $e->getResponse()->setStatusCode(400);
        }

		$follower_container = new Follower;

		$status = $this->isFollowing($data);

		if(count($status)>0){

			$res = array(

				'status' => 'conflict',
				'error' => 'You are already following the user.'
			);

			return response( array('serviceException'  => $res ), 409);
		}

		$follower_container->follower 	= $follower;
		$follower_container->following 	= $following;

		$follower_container->save();

		return response(array('status' => 'success'), 201);

    }


    // Fetch all followings.

    public function retrieveFollowings(Request $request){

    	$follower = $request->user()->userid;

    	$follower_container = new Follower;

    	$followings = $follower_container->where('follower', $follower)
    										->select('following')
    											->get();

    	if(count($followings)>0){

	    	foreach($followings as $following){

	    		$res[] = $following['following'];
	    	}	

	    } else {

	    	$res = NULL;
	    }

	    return array('following' => $res);

    }




    // Check if already following from user side.


    public function checkRelation(Request $request, $followingid){

    	$follower 			= $request->user()->userid;
    	$following 		 	= $followingid;

    	$data = [
    				'follower' 	=> $follower, 
    				'following' => $following 
    			];

    	// try{

     //        $this->validateRequest($request);

     //    } catch(ValidationException $e){

     //        return $e->getResponse()->setStatusCode(400);
     //    }

    	
		$status = $this->isFollowing($data);

		if(count($status)>0){

			return response( "", 200);
		
		} else {

			return response( "", 404 );
		}

    }



    // Check if already following.


    private function isFollowing($data){

    	$follower_container = new Follower;

    	$status = $follower_container->where('follower', $data['follower'])
    									->Where('following', $data['following'])
    										->first();

    	return $status;

    }


    private function validateRequest($request){

        $rules = [
            
            'followingid' => 'bail|required|max:100|exists:members,userid',

        ];

        $this->validate($request, $rules);

    }

}
